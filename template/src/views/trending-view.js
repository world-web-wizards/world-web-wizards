import { paginationConfig } from '../config/pagination.config.js';
import { renderFavoriteStatus } from '../helpers/helpers.js';

/**
 * @param {object} gifs list of all trending
 * @param {number} page how many pages to be
 * @return {string} all trending with paging
 */
export const toTrendingView = (gifs, page = 0) => {
  const pageCount = Math.ceil(gifs.data.length/paginationConfig.gifsPerPage);
  gifs.data = gifs.data.slice(page * paginationConfig.gifsPerPage, (page + 1) * paginationConfig.gifsPerPage);
  return `
<div class ="gifche">
  <h2 class="content-title"> Trending gifs:</h2>
  ${gifs.data.map(singleGif).join('\n')}
</div>

<div class="page-link-container">
  ${Array.from({ length: pageCount }).map((_, i)=> `<a href="#" class="page-link ${page === i ? 'selected' : ''}" data-page="${i}">${i + 1}</a>`).join('\n')}
</div>
`;
};

/**
 * @param {object} gif
 * @return {string} every single gif
 */
export const singleGif = (gif) => `
  <div class = "gif-combined-view" >
   <img class = "single-gif" src = ${gif.images.original.url}/>
   <div class = "gif-detailed">
    <a href = "#" class = "gif-details" data-gif-id = "${gif.id}"> View Details </a> 
   ${renderFavoriteStatus(gif.id)} 
   </div>
  </div>
  `;
