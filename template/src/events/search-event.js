import { CONTAINER_SELECTOR } from '../common/constants.js';
import { q } from '../helpers/helpers.js';
import { loadSearchGif } from '../requests/request-services.js';
import { toSearchView } from '../views/search-view.js';

/**
 * @param {string} searchItem the searched string
 * gives us the search view
 */
export const renderSearchGifs = async (searchItem) => {

  const searched = await loadSearchGif(searchItem);

  q(CONTAINER_SELECTOR).innerHTML = toSearchView(searched);

};
