import { renderFavoriteStatus } from '../helpers/helpers.js';

/**
 * @param {object} gifs gif we searched
 * @return {string} some similar gifs
 */
export const toSearchView = (gifs) => `
<div class ="gifche">
${gifs.data.map(singleGif).join('\n')}
</div>
`;

/**
 * @param {object} gif single gif connected to the others
 * @return {string} gives single gif info
 */
const singleGif = (gif) => `
   <div class="gif-combined-view">
      <img src = ${gif.images.original.url}/>
      <div class="gif-detailed">
         <a href = "#" class = "gif-details" data-gif-id = "${gif.id}"> View Details </a> ${renderFavoriteStatus(gif.id)}
      </div>
   </div>
  `;
