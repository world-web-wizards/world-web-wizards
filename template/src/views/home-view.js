/**
 * @return {string} home view with the logo of the team
 */
export const toHomeView = () => `
<div class="bg">

<img src = "./images/new-logo-3.png" class = "home-img"/>

</div>

<div class="star-field">
<div class="layer"></div>
<div class="layer"></div>
<div class="layer"></div>
`;
