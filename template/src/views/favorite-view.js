import { renderFavoriteStatus } from '../helpers/helpers.js';

/**
 * @param {object} gifs list of all favorite gifs
 * @return {string} list of all favorite gifs
 */
export const toFavoriteView = (gifs) => `
<div class ="favorite-gif">
<h2 class="content-title"> Your favorites gifs:</h2>
 ${gifs.data.map(singleGif).join('')}
</div>
`;

/**
 * @param {object} gif full object of a exact gif with name, id , and url
 * @return {string} every single gif in the favorite view
 */
export const singleGif = (gif) => `
  <div class = "gif-combined-view">
   <img class="favorite-single-gif" src = '${gif.images.original.url}'/>
   <div class = "gif-detailed">
    <a href = "#"  class = "gif-details" data-gif-id = "${gif.id}"> View Details </a> 
    ${renderFavoriteStatus(gif.id)} 
   </div>
</div>
  `;

/**
   * @param {object} gif random gif
   * @return {string} when empty it adds a random gif on the page
   */
export const toFavoriteEmptyView = (gif) => `
  <div class ="favorite-gif">
  <h2 class="content-title"> You do not have any favorite gifs yet! <br> Here is one random gif for you:</h2>
  <div class="gif-combined-view">
    <img class="favorite-single-gif" src = '${gif.data.images.original.url}'/>
    <div class = "gif-detailed">
      <a href = "#" class = "gif-details" data-gif-id = "${gif.data.id}"> View Details </a> 
      ${renderFavoriteStatus(gif.data.id)}
    </div>
  </div>
</div>
  `;
