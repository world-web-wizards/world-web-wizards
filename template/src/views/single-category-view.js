/**
 * @param {Array} subcategories list of all subcategories
 * @return {string} list with all subcategories
 */
export const toSubCategoriesView = (subcategories) => `
    <div class="subcategory-div">
        <h2 class="content-subcategory-title">${subcategories[0].name.toUpperCase()}</h2>
    </div>
    <div class="subcategory">   
    ${subcategories[0].subcategories.map(toSingleSubCategory).join('')}
    </div>
`;

/**
 * @param {object} subcategory
 * @return {string} link list of all subcategories
 */
export const toSingleSubCategory = (subcategory) => `
<a href= "#" class= "single-sub-category" data-subcategory="${subcategory.name}"> ${subcategory.name} </a>
`;

