import { renderFavoriteStatus } from '../helpers/helpers.js';

/**
 * @param {object} gif a single gif
 * @return {string} gives all details of the wanted gif
 */
export const detailedGifView = (gif) => {
  return `
  <div class="detailes-view">
    <div class="details-up">
      <div class = "left-side">
          <img src = ${gif.data.images.original.url}/>
        </div>

        <div class = "right-side">
          <p>Gif Name: ${gif.data?.title || 'Unknown Title'}</p>
          <p>Uploaded By: ${gif.data.user?.display_name || 'Unknown User'}</p>
          <p>Date Uploaded: ${gif.data?.import_datetime || 'Unknown Date'}</p>
          <p class="add-to-favorite"> ${renderFavoriteStatus(gif.data.id)} </p>
        </div>
    </div>
    <h2 class="content-title" id="related-title"> Related gifs:</h2>
    <div id = "related-gifs"></div>
  </div>

`;
};
