/**
 * @param {object} categories with key data we get the array of subcategories
 * @return {string} category list view with all subcategories
 */
export const toCategoriesView = (categories) => `
    ${categories.data.map(toSingleCategory).join('\n')}
`;

/**
 * @param {object} category subcategory
 * @return {string} subcategory list
 */
export const toSingleCategory = (category) => `
     <li><a href="#" class="category-gifs" data-category-name="${category.name}">${category.name.toUpperCase()}</li>
 `;
