import { apiKey } from '../common/constants.js';

/**
 * @return {object} all trending gifs with API_KEY
 */
export const loadTrendingGifs = async () => {

  const response = await fetch(`https://api.giphy.com/v1/gifs/trending?api_key=${apiKey}&limit=50`);

  return await response.json();

};

/**
 * @param {string} arrayIDs string of the favorite id
 * @return {object} when full it gets the wanted gifs if empty gives empty gif
 */
export const loadFavoriteGifs = async (arrayIDs) => {

  if (arrayIDs.length === 0) {
    const empty = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
    return await empty.json();
  } else {
    const ids = arrayIDs.join(',');
    const result = await fetch(`https://api.giphy.com/v1/gifs?api_key=${apiKey}&ids=${ids}`);
    return await result.json();
  }
};

/**
 * @param {string} id gives the gif we need
 * @return {object}
 */
export const loadSingleGif = async (id = '') => {

  const singleGifID = await fetch(`https://api.giphy.com/v1/gifs/${id}?api_key=${apiKey}`);

  return await singleGifID.json();

};

/**
 * @param {string} text searched text
 * @return {object} all gifs that include the text
 */
export const loadSearchGif = async (text) => {

  const searchedGif = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=${apiKey}&q=${text}&limit=24`);

  return await searchedGif.json();

};

/**
 * @return {object} with all categories
 */
export const loadCategories = async () => {

  const categories = await fetch(`https://api.giphy.com/v1/gifs/categories?api_key=${apiKey}`);

  return await categories.json();

};


