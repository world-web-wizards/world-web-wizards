import { q } from '../../helpers/helpers.js';
import { uploadedSuccessfully } from '../../views/notification-popup-view.js';
import { NOTIFICATION } from '../constants.js';

const uploaded = JSON.parse(localStorage.getItem('my-gifs')) || [];

/**
 * @param {string} uploadedId id we generate to upload
 */
export const addUploaded = (uploadedId) => {

  uploaded.push(uploadedId);
  localStorage.setItem('my-gifs', JSON.stringify(uploaded));

  q(NOTIFICATION).innerHTML = uploadedSuccessfully();

};

export const getUploaded = () => [...uploaded];
