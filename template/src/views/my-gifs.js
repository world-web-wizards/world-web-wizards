import { renderFavoriteStatus } from '../helpers/helpers.js';

/**
 * @param {object} gifs uploaded gif from us
 * @return {string} to my view with all uploaded gifs
 */
export const toMyGifsView = (gifs) => `
<div class ="my-gifs favorite-gif">
  <h2 class="content-title">My uploaded gifs:</h2>
 
    ${gifs.data.map(singleGif).join('')}

</div>
`;

/**
 * @param {object} gif single gif uploaded from us
 * @return {string} all info per gif
 */
export const singleGif = (gif) => `
<div class = "gif-combined-view">
   <img class="favorite-single-gif" src = '${gif.images.original.url}'/>
   <div class = "gif-detailed">
<a href = "#" class = "gif-details" data-gif-id = "${gif.id}"> View Details </a> ${renderFavoriteStatus(gif.id)}
</div>
</div>
  `;

/**
 * @return {string} my gifs when it is empty
 */
export const toEmptyView = () => `
  <div class ="favorite-gif">
  <h2 class="content-title"> You do not have any uploaded gifs!</h2>
</div>
</div>
  `;
