export const HOME = 'home';

export const UPLOAD = 'upload';

export const TRENDING = 'trending';
export const FAVORITES = 'favorites';

export const ABOUT = 'about';

export const CONTAINER_SELECTOR = '#container';

export const FULL_HEART = '❤';

export const EMPTY_HEART = '♡';

export const apiKey = 'nQPWqZgG5Rxjm7LwSiG3wPNcUsx1bGti';

export const MY_GIFS = 'my-gifs';

export const CATEGORIES_SELECTOR = '#categories';

export const RELATED_GIFS = '#related-gifs';

export const NOTIFICATION = '#notification';
