/**
 * @return {string} upload error
 */
export const expectGifImage = () => `
 <img src = "./images/x.png" class = "failed-upload"/>
<p class = "failed-text"> Expected gif image </p>
`;

/**
 * @return {string} upload error
 */
export const unableToUploadFile = () => `
 <img src = "./images/x.png" class = "failed-upload"/>
<p class = "failed-text"> Unable to upload file </p>
`;

/**
 * @return {string} uploaded successfully
 */
export const uploadedSuccessfully = () => `
 <img src = "./images/check.png" class = "success-upload"/>
<p class ="success-text"> File uploaded successfully </p>
`;
