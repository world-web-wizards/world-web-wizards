/**
 * @return {string} about view with info of the owners
 */
export const toAboutView = () => `
<div id="about">
  <div class="content">
    <h1 class = "about-titles">World Web Wizards</h1>
    <h2 class = "about-titles">Authors: Group Project 1, A36 JS</h2>

    <div class="authors">
      <div class = "single-author">
        <img class="author-img" src="./images/Toni.jpg">
        <p class="author-name"> Antoniya Lambova </p>
        <a href="mailto:adelfia@gmail.com" class="email">E-mail: adelfia@gmail.com</a>
        <p><a href="tel:+359887985456" class="phone">Phone number: +359 887 985 456</p>
      </div>
      <div class = "single-author">
        <img class="author-img" src="./images/Mario.png">
        <p class="author-name"> Mario Vangelov </p>
        <a href="mailto:mario@gmail.com" class="email">E-mail: mario@gmail.com</a>
        <p><a href="tel:+3598879865123" class="phone">Phone number: +359 888 123 564</p>
      </div>
      <div class = "single-author">
        <img class="author-img" src="./images/Zhorzh.png">
        <p class="author-name"> Zhorzh Tomov </p>
        <a href="mailto:zhorzh@gmail.com" class="email">E-mail: zh_tomov@hotmail.com</a>
        <p><a href="tel:+3598879865123" class="phone">Phone number: +359 886 854 333</p>
      </div>
    </div>
  </div>
</div>
`;
