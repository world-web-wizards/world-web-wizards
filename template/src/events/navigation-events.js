import { HOME, ABOUT, FAVORITES, UPLOAD, TRENDING, CONTAINER_SELECTOR, apiKey, MY_GIFS, CATEGORIES_SELECTOR, RELATED_GIFS, NOTIFICATION } from '../common/constants.js';
import { q, setActiveNav } from '../helpers/helpers.js';
import { loadCategories, loadFavoriteGifs, loadSearchGif, loadSingleGif, loadTrendingGifs } from '../requests/request-services.js';
import { toHomeView } from '../views/home-view.js';
import { toTrendingView } from '../views/trending-view.js';
import { toAboutView } from '../views/about-view.js';
import { toUploadView } from '../views/upload-view.js';
import { detailedGifView } from '../views/gif-view.js';
import { getFavorites } from '../common/data/favorite.js';
import { toFavoriteEmptyView, toFavoriteView } from '../views/favorite-view.js';
import { toCategoriesView } from '../views/category-view.js';
import { toEmptyView, toMyGifsView } from '../views/my-gifs.js';
import { addUploaded, getUploaded } from '../common/data/uploaded.js';
import { toSubCategoriesView } from '../views/single-category-view.js';
import { toSearchView } from '../views/search-view.js';
import { expectGifImage, unableToUploadFile } from '../views/notification-popup-view.js';

/**
 * @param {string} page the page we want
 * @return {PageTransitionEvent} the correct load to do
 */
export const loadPage = (page = '') => {

  switch (page) {

  case HOME:
    setActiveNav(HOME);
    return renderHome();

  case FAVORITES:
    setActiveNav(FAVORITES);
    return renderFavorites();

  case TRENDING:
    setActiveNav(TRENDING);
    return renderTrending();

  case UPLOAD:
    setActiveNav(UPLOAD);
    return renderUpload();

  case MY_GIFS:
    setActiveNav(MY_GIFS);
    return renderMyGifs();

  case ABOUT:
    setActiveNav(ABOUT);
    return renderAbout();


  default: return null;
  }
};

/**
 * it prints the home page when necessary
 */
export const renderHome = () => {

  q(CONTAINER_SELECTOR).innerHTML = toHomeView();

};

/**
 * @param {string} gifID gets the id and get us to detailed view
 */
export const renderSingleGif = async (gifID = '') => {

  const loadedSingleGif = await loadSingleGif(gifID);

  const loadedRelatedGifs = loadedSingleGif.data.title.split(' GIF');

  const relatedGifs = loadedRelatedGifs[0];

  const loadSearchedRelated = await loadSearchGif(relatedGifs);

  q(CONTAINER_SELECTOR).innerHTML = detailedGifView(loadedSingleGif);

  q(RELATED_GIFS).innerHTML = toSearchView(loadSearchedRelated);

};

/**
 * gives the trending view page
 */
export const renderTrending = async () => {

  const trendingGifs = await loadTrendingGifs();
  q(CONTAINER_SELECTOR).innerHTML = toTrendingView(trendingGifs);

};

/**
 * @param {number} page
 * gives the trending view when it is with pages
 */
export const loadGifsPageView = async (page) => {
  const trendingGifs = await loadTrendingGifs();

  q(CONTAINER_SELECTOR).innerHTML = toTrendingView(trendingGifs, page);
};

/**
 * gives the about view
 */
export const renderAbout = () => {

  q(CONTAINER_SELECTOR).innerHTML = toAboutView();

};

/**
 * gives the favorite view when it is empty and when it is not
 */
export const renderFavorites = async () => {

  const allFavorites = getFavorites();

  const favoriteGifs = await loadFavoriteGifs(allFavorites);

  if (allFavorites.length === 0) {

    q(CONTAINER_SELECTOR).innerHTML = toFavoriteEmptyView(favoriteGifs);

  } else {

    q(CONTAINER_SELECTOR).innerHTML = toFavoriteView(favoriteGifs);

  }
};

/**
 * gives the category view
 */
export const renderCategories = async () => {

  const allCategories = await loadCategories();

  q(CATEGORIES_SELECTOR).innerHTML = toCategoriesView(allCategories);

};

/**
 * @param {string} subcategory
 * takes all subcategories in a category
 */
export const renderSubCategories = async (subcategory) => {

  const allCategories = await loadCategories();

  const singleCategory = allCategories.data.filter(e=> e.name === subcategory);

  q(CONTAINER_SELECTOR).innerHTML = toSubCategoriesView(singleCategory);

};

/**
 * @param {string} subcategory
 * gives all related gives in the subcategory
 */
export const renderSubCategory = async (subcategory) => {

  const searched = await loadSearchGif(subcategory);

  q(CONTAINER_SELECTOR).innerHTML = toSearchView(searched);

};

/**
 * gives my gifs view with all the gifs we upload when we have and when we have not
 */
export const renderMyGifs = async () => {

  const allUploaded = getUploaded();

  const uploadedGifs = await loadFavoriteGifs(allUploaded);

  if (allUploaded.length === 0) {

    q(CONTAINER_SELECTOR).innerHTML = toEmptyView();

  } else {

    q(CONTAINER_SELECTOR).innerHTML = toMyGifsView(uploadedGifs);

  }
};


/**
 * gives us the upload view and upload menu to be able to upload gifs
 */
export const renderUpload = () => {

  q(CONTAINER_SELECTOR).innerHTML = toUploadView();

  q('#submit-form-btn').addEventListener('click', async (e) => {

    e.preventDefault();

    const form = q('#upload-form');

    const formData = new FormData(form);

    formData.append('api_key', apiKey);


    if (formData.get('file').type !== 'image/gif') {

      q(NOTIFICATION).innerHTML = expectGifImage();

      return;
    }

    try {

      // const loadingGif = await loadSingleGif('11ASZtb7vdJagM');
      q(NOTIFICATION).innerHTML = `<img src = "./images/loading3.gif" class ="loading-gif"/>`;

      const response = await fetch('https://upload.giphy.com/v1/gifs', {
        method: 'POST',
        body: formData,
      });

      const json = await response.json();

      const uploadedId = json.data.id;

      addUploaded(uploadedId);

    } catch (err) {

      q(NOTIFICATION).innerHTML = unableToUploadFile();

    }
  });
};
