import { addFavorite, getFavorites, removeFavorite } from '../common/data/favorite.js';
import { q, renderFavoriteStatus } from '../helpers/helpers.js';


/**
 * @param {string} gifId check if the gif is already in favorites if not it add if yes it delete
 */
export const toggleFavoriteStatus = (gifId) => {
  const favorites = getFavorites();

  if (favorites.includes(gifId)) {
    removeFavorite(gifId);
  } else {
    addFavorite(gifId);
  }

  const parent = q(`span[data-gif-id = "${gifId}"]`).parentElement;
  parent.removeChild(parent.lastElementChild);
  parent.insertAdjacentHTML('beforeend', renderFavoriteStatus(gifId));
};
