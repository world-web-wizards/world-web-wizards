/**
 * @return {string} upload view
 */
export const toUploadView = () => `
<div id="upload">
  <div class="content">
  <h2 class="gif-upload-title"> Choose a gif to upload:</h2>
    <form id="upload-form" class="upload-btn">
    <input type="file" name="file" id = "file" class="choose-file"></input>
    <label for ="file"> Choose file </label>
        <button type="submit" id="submit-form-btn" class="submit-btn">Submit</button>
    </form>
    
    <div id="notification" class="notification">

    </div>
  </div>
</div>
`;
