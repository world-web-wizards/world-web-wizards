# <h1 align ="center"> **_World Web Wizards_** </h1>
![logo](/uploads/5049b6c12e54d73654175de5268cee2c/logo.png)
### **Group 1 - Web Project**

## **Group project Alpha Version Look**
![alpha-site-version](/uploads/57b03dbec02581a8b95f564223128f9d/alpha-site-version.png)

## **HOME PAGE**

![image.png](./image.png)

## **TRENDING PAGE**
![image-1.png](./image-1.png)

## **TRENDING PAGE - BOTTOM**

![image-2.png](./image-2.png)

## **FAVORITES PAGE - (NO FAVORITE GIFS ADDED)**

![image-3.png](./image-3.png)

## **FAVORITES PAGE - (WITH ADDED FAVORITE GIFS)**

![image-4.png](./image-4.png)

## **GIF DETAILS PAGE** 

![image-5.png](./image-5.png)

## **RELATED GIFS VIEW**

![image-6.png](./image-6.png)

## **UPLOAD PAGE**

![image-7.png](./image-7.png)
![image-8.png](./image-8.png)
![image-9.png](./image-9.png)

## **MY GIFS PAGE**

![image-10.png](./image-10.png)

## **ABOUT PAGE**

![image-11.png](./image-11.png)

## **SEARCH VIEW**

![image-12.png](./image-12.png)

## **CATEGORIES / SUB-CATEGORIES**

![image-13.png](./image-13.png)

**OPENED SUB-CATEGORY**

![image-14.png](./image-14.png)
