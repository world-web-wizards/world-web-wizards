let favorites = JSON.parse(localStorage.getItem('favorite')) || [];

/**
 * @param {string} gifId id of gif we want
 */
export const addFavorite = (gifId) => {
  if (favorites.find(id => id === gifId)) return;


  favorites.push(gifId);
  localStorage.setItem('favorite', JSON.stringify(favorites));

};

/**
 * @param {string} gifId id that we want to remove
 */
export const removeFavorite = (gifId) => {
  favorites = favorites.filter(id => id !== gifId);
  localStorage.setItem('favorite', JSON.stringify(favorites));
};

export const getFavorites = () => [...favorites];
