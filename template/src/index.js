import { HOME } from './common/constants.js';
import { q } from './helpers/helpers.js';
import { loadGifsPageView, loadPage, renderCategories, renderSingleGif, renderSubCategories, renderSubCategory } from './events/navigation-events.js';
import { renderSearchGifs } from './events/search-event.js';
import { toggleFavoriteStatus } from './events/favorites-event.js';


document.addEventListener('DOMContentLoaded', () => {

  document.addEventListener('click', event => {


    if (event.target.classList.contains('nav-link')) {
      loadPage(event.target.getAttribute('data-page'));
    }

    if (event.target.classList.contains('gif-details')) {
      renderSingleGif(event.target.getAttribute('data-gif-id'));
    }

    if (event.target.classList.contains('favorite')) {
      toggleFavoriteStatus(event.target.getAttribute('data-gif-id'));
    }

    if (event.target.classList.contains('category-gifs')) {
      renderSubCategories(event.target.getAttribute('data-category-name'));
    }

    if (event.target.classList.contains('single-sub-category')) {
      renderSubCategory(event.target.getAttribute('data-subcategory'));
    }

    if (event.target.classList.contains('page-link')) {
      loadGifsPageView(+event.target?.dataset.page);
    }

  });


  // search events
  q('#search').addEventListener('input', event => {
    renderSearchGifs(event.target.value);
  });

  loadPage(HOME);
  renderCategories();

});
